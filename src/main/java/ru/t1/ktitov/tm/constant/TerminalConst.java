package ru.t1.ktitov.tm.constant;

public final class TerminalConst {

    public final static String VERSION = "version";

    public final static String HELP = "help";

    public final static String ABOUT = "about";

    public final static String EXIT = "exit";

    public final static String INFO = "info";

    public final static String ARGUMENTS = "arguments";

    public final static String COMMANDS = "commands";

}
