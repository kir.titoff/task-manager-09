package ru.t1.ktitov.tm;

import ru.t1.ktitov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
