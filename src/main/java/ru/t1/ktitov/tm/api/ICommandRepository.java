package ru.t1.ktitov.tm.api;

import ru.t1.ktitov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
