package ru.t1.ktitov.tm.component;

import ru.t1.ktitov.tm.api.ICommandController;
import ru.t1.ktitov.tm.api.ICommandRepository;
import ru.t1.ktitov.tm.api.ICommandService;
import ru.t1.ktitov.tm.constant.ArgumentConst;
import ru.t1.ktitov.tm.constant.TerminalConst;
import ru.t1.ktitov.tm.controller.CommandController;
import ru.t1.ktitov.tm.repository.CommandRepository;
import ru.t1.ktitov.tm.service.CommandService;
import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) {
            commandController.showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                commandController.showErrorCommand(command);
        }
    }

    private void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter command: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument(argument);
        }
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void exit() {
        System.exit(0);
    }

    public void run(final String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

}
